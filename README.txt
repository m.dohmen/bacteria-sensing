Stochastic simulation of sensor responses for bacteria differentiation and
classification.

The functions are found in sim_functions.py and can be run with the script in
paper-images.py

For a number of bacteria species n_B a response pattern for the n_S
sensors is randomly generated in a given range of responses modeled after values
from the measurements. In this simulation up to n_B = 10 bacteria and
n_S = 25 sensors were initiated. The response of the sensor set is either a uniformly
positive or negative response (sensor responses = 0.7–1.7). A given number of
sensors per set produce the same response pattern as they do for another bacterium,
therefore are set to the same random value. According to the measurement
data, approximately 40% of the sensors had an indistinguishable response compared
the dataset of another bacterium. The response of the rest of the sensors was
randomly chosen within the known experimental range for the different bacteria.
To the response matrix a random noise is applied r times to account for experiment
repetition. The noise observed in the data was up to 10%. For the principal
component analysis (PCA), the number of sensors is equal to the number of
principal components (PC), therefore the experiment was virtually repeated 25
times. When considering a rising number of PC, the response matrix is generated
in its entirety and a rising number of senor entries are used in the calculation to
model the development of additional sensors. The number of features in a PCA
must be equal or exceed the number of PC, therefore r is equal to n_S to generate a
doubled dataset for training and testing the PCA. The PCA as implemented in
scikit-learn is solved with a full singular value decomposition (SVD) and a
logistic regression with a bilinear solver and is used to predict the bacteria species
of the test dataset. The percentage of correctly assigned test cases can be calculated
with a confusion matrix which has correct assignments as its diagonal elements.
Used Python packages: matplotlib (version 3.0.3), NumPy (version 1.16.2),
scikit-learn (version 0.20.3), Pandas (version 0.24.2).